//
// This file is part of simpleVec.
//
// (c) 2020 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef SIMPLEVEC_VECTOR_H
#define SIMPLEVEC_VECTOR_H

#include <cassert>
#include <limits>
#include <cmath>
#include <ostream>

#ifndef SIMPLEVEC_CUDA_HOST_DEVICE
	#ifdef SIMPLEVEC_USE_CUDA
		#include <cuda_runtime.h>
		#define SIMPLEVEC_CUDA_HOST_DEVICE __host__ __device__
	#else
		#define SIMPLEVEC_CUDA_HOST_DEVICE
	#endif
#endif

/// \file vector.h
/// \brief This vector library was originally conceived for Brane (see https://sebastiengarmier.ch/brane)

namespace SimpleVec {

// Cardinal angles

enum CardinalAngle {
	DEG_0,
	DEG_90,
	DEG_180,
	DEG_270
};

static CardinalAngle CWCardinalRotation(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_270;
		case DEG_90:
			return DEG_0;
		case DEG_180:
			return DEG_90;
		case DEG_270:
			return DEG_180;
		default:
			return DEG_270;
	}
}

static CardinalAngle CCWCardinalRotation(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_90;
		case DEG_90:
			return DEG_180;
		case DEG_180:
			return DEG_270;
		case DEG_270:
			return DEG_0;
		default:
			return DEG_90;
	}
}

static CardinalAngle OppositeCardinalAngle(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_180;
		case DEG_90:
			return DEG_270;
		case DEG_180:
			return DEG_0;
		case DEG_270:
			return DEG_90;
		default:
			return DEG_0;
	}
}

static CardinalAngle MirrorCardinalAngle(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return DEG_0;
		case DEG_90:
			return DEG_270;
		case DEG_180:
			return DEG_180;
		case DEG_270:
			return DEG_90;
		default:
			return DEG_0;
	}
}

static int32_t CardinalSin(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return 0;
		case DEG_90:
			return 1;
		case DEG_180:
			return 0;
		case DEG_270:
			return -1;
		default:
			return 0;
	}
}

static int32_t CardinalCos(CardinalAngle angle) {
	switch(angle) {
		case DEG_0:
			return 1;
		case DEG_90:
			return 0;
		case DEG_180:
			return -1;
		case DEG_270:
			return 0;
		default:
			return 1;
	}
}

#ifndef SIMPLEVEC_USE_ONLY_VECTOR4

// Vector2<T>

template<typename T>
class Vector2 {

public:

	union {
		struct {
			union {
				T m_1;
				T a;
				T x;
			};
			union {
				T m_2;
				T b;
				T y;
			};
		};
		T m_raw[2];
	};

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2() : Vector2(0, 0) {};
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2(T x, T y) : x(x), y(y) {}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2(Vector2<T> const& vector) {
		x = vector.x;
		y = vector.y;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2(Vector2<T> const&& vector) {
		x = vector.x;
		y = vector.y;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2(CardinalAngle angle) {
		switch(angle) {
			case DEG_0:
				x = T(1); y = T(0);
				break;
			case DEG_90:
				x = T(0); y = T(1);
				break;
			case DEG_180:
				x = T(-1); y = T(0);
				break;
			case DEG_270:
				x = T(0); y = T(-1);
				break;
		}
	}

	template<typename U>
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator Vector2<U>() const {
		return Vector2<U>((U)x, (U)y);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator=(Vector2<T> const& vector) {
		x = vector.x;
		y = vector.y;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator=(Vector2<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		return *this;
	}

#ifdef SIMPLEVEC_USE_CUDA
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator==(Vector2<T> const& vector) const {
		return (x == vector.x
			&& y == vector.y);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator!=(Vector2<T> const& vector) const {
		return (x != vector.x
			&& y != vector.y);
	}	
#else
	bool operator==(Vector2<T> const& vector) const {
		return (std::abs(x - vector.x) < std::numeric_limits<T>::epsilon()
			&& std::abs(y - vector.y) < std::numeric_limits<T>::epsilon());
	}
	bool operator!=(Vector2<T> const& vector) const {
		return (std::abs(x - vector.x) > std::numeric_limits<T>::epsilon()
			|| std::abs(y - vector.y) > std::numeric_limits<T>::epsilon());
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator+(Vector2<T> const& vector) const {
		return{ x + vector.x, y + vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator+(Vector2<T> const&& vector) const {
		return{ x + vector.x, y + vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator-(Vector2<T> const& vector) const {
		return{ x - vector.x, y - vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator*(Vector2<T> const& vector) const {
		return{ x * vector.x, y * vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator/(Vector2<T> const& vector) const {
		return{ x / vector.x, y / vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator-() const {
		return{ -x, -y };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator+=(Vector2<T> const& vector) {
		x += vector.x;
		y += vector.y;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator-=(Vector2<T> const& vector) {
		x -= vector.x;
		y -= vector.y;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator*=(Vector2<T> const& vector) {
		x *= vector.x;
		y *= vector.y;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator/=(Vector2<T> const& vector) {
		x /= vector.x;
		y /= vector.y;
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator+(T const& scalar) const {
		return{ x + scalar, y + scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator-(T const& scalar) const {
		return{ x - scalar, y - scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator*(T const& scalar) const {
		return{ scalar * x, scalar * y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator/(T const& scalar) const {
		return{ x / scalar, y / scalar };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator+=(T const& scalar) {
		x += scalar;
		y += scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator-=(T const& scalar) {
		x -= scalar;
		y -= scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator*=(T const& scalar) {
		x *= scalar;
		y *= scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T>& operator/=(T const& scalar) {
		x /= scalar;
		y /= scalar;
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	T Length() const {
		return (T)std::sqrt(x * x + y * y);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T Length2() const {
		return x * x + y * y;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> Normalize() const {
		T len = Length();
		return{ x / len, y / len };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> Normal() const {
		return{ -y, x };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	T Dot(Vector2<T> const& vector) const {
		return x * vector.x + y * vector.y;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T Cross(Vector2<T> const& vector) const {
		return x * vector.y - y * vector.x;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T*() {
		return (T*)&x;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T const*() const {
		return (T const*)&x;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector2<T> operator+(T const& scalar, Vector2<T> const& vector) {
		return{ scalar + vector.x, scalar + vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector2<T> operator-(T const& scalar, Vector2<T> const& vector) {
		return{ scalar - vector.x, scalar - vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector2<T> operator*(T const& scalar, Vector2<T> const& vector) {
		return{ scalar * vector.x, scalar * vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector2<T> operator/(T const& scalar, Vector2<T> const& vector) {
		return{ scalar / vector.x, scalar / vector.y };
	}
	
	friend std::ostream& operator<<(std::ostream& stream, Vector2<T> const& vector) {
		stream << "[" << vector.x << "," << vector.y << "]";
		return stream;
	}
};

// Typedefs for Vector2<T>

typedef Vector2<float> Vector2_float;
typedef Vector2<double> Vector2_double;

// Vector3<T>

template<typename T>
class Vector3 {

public:

	union {
		struct {
			union {
				T m_1;
				T a;
				T x;
			};
			union {
				T m_2;
				T b;
				T y;
			};
			union {
				T m_3;
				T c;
				T z;
			};
		};
		T m_raw[3];
	};

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3() : Vector3(0, 0, 0) {};
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3(T x, T y, T z) : x(x), y(y), z(z) {}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3(Vector3<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3(Vector3<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	template<typename U>
	operator Vector3<U>() const {
		return Vector3<U>((U)x, (U)y, (U)z);
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator=(Vector3<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator=(Vector3<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		return *this;
	}

#ifdef SIMPLEVEC_USE_CUDA
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator==(Vector3<T> const& vector) const {
		return (x == vector.x
			&& y == vector.y
			&& z == vector.z);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator!=(Vector3<T> const& vector) const {
		return (x != vector.x
			&& y != vector.y
			&& z != vector.z);
	}	
#else
	bool operator==(Vector3<T> const& vector) const {
		return (std::abs(x - vector.x) < std::numeric_limits<T>::epsilon()
			&& std::abs(y - vector.y) < std::numeric_limits<T>::epsilon()
			&& std::abs(z - vector.z) < std::numeric_limits<T>::epsilon());
	}
	bool operator!=(Vector3<T> const& vector) const {
		return (std::abs(x - vector.x) > std::numeric_limits<T>::epsilon()
			|| std::abs(y - vector.y) > std::numeric_limits<T>::epsilon()
			|| std::abs(z - vector.z) > std::numeric_limits<T>::epsilon());
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator+(Vector3<T> const& vector) const {
		return{ x + vector.x, y + vector.y, z + vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator-(Vector3<T> const& vector) const {
		return{ x - vector.x, y - vector.y, z - vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator*(Vector3<T> const& vector) const {
		return{ x * vector.x, y * vector.y, z * vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator/(Vector3<T> const& vector) const {
		return{ x / vector.x, y / vector.y, z / vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator-() const {
		return{ -x, -y, -z };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator+=(Vector3<T> const& vector) {
		x += vector.x;
		y += vector.y;
		z += vector.z;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator-=(Vector3<T> const& vector) {
		x -= vector.x;
		y -= vector.y;
		z -= vector.z;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator*=(Vector3<T> const& vector) {
		x *= vector.x;
		y *= vector.y;
		z *= vector.z;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator/=(Vector3<T> const& vector) {
		x /= vector.x;
		y /= vector.y;
		z /= vector.z;
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator+(T const& scalar) const {
		return{ x + scalar, y + scalar, z + scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator-(T const& scalar) const {
		return{ x - scalar, y - scalar, z - scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator*(T const& scalar) const {
		return{ scalar * x, scalar * y, z * scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator/(T const& scalar) const {
		return{ x / scalar, y / scalar, z / scalar };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator+=(T const& scalar) {
		x += scalar;
		y += scalar;
		z += scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator-=(T const& scalar) {
		x -= scalar;
		y -= scalar;
		z -= scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator*=(T const& scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T>& operator/=(T const& scalar) {
		x /= scalar;
		y /= scalar;
		z /= scalar;
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	T Length() const {
		return (T)std::sqrt(x * x + y * y + z * z);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T Length2() const {
		return x * x + y * y + z * z;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> Normalize() const {
		T len = Length();
		return{ x / len, y / len, z / len };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	T Dot(Vector3<T> const& vector) const {
		return x * vector.x + y * vector.y + z * vector.z;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> Cross(Vector3<T> const& vector) const {
		return{ y * vector.z - z * vector.y,
			z * vector.x - x * vector.z,
			x * vector.y - y * vector.x };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T*() {
		return (T*)&x;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T const*() const {
		return (T const*)&x;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector3<T> operator+(T const& scalar, Vector3<T> const& vector) {
		return{ scalar + vector.x, scalar + vector.y, scalar + vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector3<T> operator-(T const& scalar, Vector3<T> const& vector) {
		return{ scalar - vector.x, scalar - vector.y, scalar - vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector3<T> operator*(T const& scalar, Vector3<T> const& vector) {
		return{ scalar * vector.x, scalar * vector.y, scalar * vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector3<T> operator/(T const& scalar, Vector3<T> const& vector) {
		return{ scalar / vector.x, scalar / vector.y, scalar / vector.z };
	}
	
	friend std::ostream& operator<<(std::ostream& stream, Vector3<T> const& vector) {
		stream << "[" << vector.x << "," << vector.y << "," << vector.z << "]";
		return stream;
	}
};

// Typedefs for Vector3<T>

typedef Vector3<float> Vector3_float;
typedef Vector3<double> Vector3_double;

#endif

/// This vector class can be used both in host and device code
template<typename T>
class Vector4 {

public:

	union {
		struct {
			union {
				T m_0;
				T a;
				T x;
			};
			union {
				T m_1;
				T b;
				T y;
			};
			union {
				T m_2;
				T c;
				T z;
			};
			union {
				T m_3;
				T d;
				T w;
			};
		};
		T m_raw[4];
	};

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4() = default;
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4(Vector4<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4(Vector4<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
	}

	template<typename U>
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator Vector4<U>() const {
		return Vector4<U>((U)x, (U)y, (U)z, (U)w);
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator=(Vector4<T> const& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator=(Vector4<T> const&& vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
		w = vector.w;
		return *this;
	}
	
#ifdef SIMPLEVEC_USE_CUDA
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator==(Vector4<T> const& vector) const {
		return (x == vector.x
			&& y == vector.y
			&& z == vector.z
			&& w == vector.w);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator!=(Vector4<T> const& vector) const {
		return (x != vector.x
			&& y != vector.y
			&& z != vector.z
			&& w != vector.w);
	}	
#else
	bool operator==(Vector4<T> const& vector) const {
		return (std::abs(x - vector.x) < std::numeric_limits<T>::epsilon()
			&& std::abs(y - vector.y) < std::numeric_limits<T>::epsilon()
			&& std::abs(z - vector.z) < std::numeric_limits<T>::epsilon()
			&& std::abs(w - vector.w) < std::numeric_limits<T>::epsilon());
	}
	bool operator!=(Vector4<T> const& vector) const {
		return (std::abs(x - vector.x) > std::numeric_limits<T>::epsilon()
			|| std::abs(y - vector.y) > std::numeric_limits<T>::epsilon()
			|| std::abs(z - vector.z) > std::numeric_limits<T>::epsilon()
			|| std::abs(w - vector.w) > std::numeric_limits<T>::epsilon());
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator+(Vector4<T> const& vector) const {
		return{ x + vector.x, y + vector.y, z + vector.z, w + vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator-(Vector4<T> const& vector) const {
		return{ x - vector.x, y - vector.y, z - vector.z, w - vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator*(Vector4<T> const& vector) const {
		return{ x * vector.x, y * vector.y, z * vector.z, w * vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator/(Vector4<T> const& vector) const {
		return{ x / vector.x, y / vector.y, z / vector.z, w / vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator-() const {
		return{ -x, -y, -z, -w };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator+=(Vector4<T> const& vector) {
		x += vector.x;
		y += vector.y;
		z += vector.z;
		w += vector.w;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator-=(Vector4<T> const& vector) {
		x -= vector.x;
		y -= vector.y;
		z -= vector.z;
		w -= vector.w;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator*=(Vector4<T> const& vector) {
		x *= vector.x;
		y *= vector.y;
		z *= vector.z;
		w *= vector.w;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator/=(Vector4<T> const& vector) {
		x /= vector.x;
		y /= vector.y;
		z /= vector.z;
		w /= vector.w;
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator+(T const& scalar) const {
		return{ x + scalar, y + scalar, z + scalar, w + scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator-(T const& scalar) const {
		return{ x - scalar, y - scalar, z - scalar, w - scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator*(T const& scalar) const {
		return{ scalar * x, scalar * y, z * scalar, w * scalar };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator/(T const& scalar) const {
		return{ x / scalar, y / scalar, z / scalar, w / scalar };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator+=(T const& scalar) {
		x += scalar;
		y += scalar;
		z += scalar;
		w += scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator-=(T const& scalar) {
		x -= scalar;
		y -= scalar;
		z -= scalar;
		w -= scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator*=(T const& scalar) {
		x *= scalar;
		y *= scalar;
		z *= scalar;
		w *= scalar;
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T>& operator/=(T const& scalar) {
		x /= scalar;
		y /= scalar;
		z /= scalar;
		w /= scalar;
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	T Length() const {
		return (T)sqrt(x * x + y * y + z * z + w * w);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T MinkowskiLength2() const {
		return -m_0 * m_0 + m_1 * m_1 + m_2 * m_2 + m_3 * m_3;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T Length2() const {
		return x * x + y * y + z * z + w * w;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> Normalize() const {
		T len = Length();
		return{ x / len, y / len, z / len, w / len };
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> CartRotX(T angle) const {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		return Vector4<T>(
			m_0,
			m_1,
			c * m_2 - s * m_3,
			s * m_2 + c * m_3
		);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> CartRotY(T angle) const {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		return Vector4<T>(
			m_0,
			c * m_1 + s * m_3,
			m_2,
			-s * m_1 + c * m_3
		);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> CartRotZ(T angle) const {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		return Vector4<T>(
			m_0,
			c * m_1 - s * m_2,
			s * m_1 + c * m_2,
			m_3
		);
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	T Dot(Vector4<T> const& vector) const {
		return x * vector.x + y * vector.y + z * vector.z + w * vector.w;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T*() {
		return (T*)&x;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T const*() const {
		return (T const*)&x;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector4<T> operator+(T const& scalar, Vector4<T> const& vector) {
		return{ scalar + vector.x, scalar + vector.y, scalar + vector.z, scalar + vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector4<T> operator-(T const& scalar, Vector4<T> const& vector) {
		return{ scalar - vector.x, scalar - vector.y, scalar - vector.z, scalar - vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector4<T> operator*(T const& scalar, Vector4<T> const& vector) {
		return{ scalar * vector.x, scalar * vector.y, scalar * vector.z, scalar * vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Vector4<T> operator/(T const& scalar, Vector4<T> const& vector) {
		return{ scalar / vector.x, scalar / vector.y, scalar / vector.z, scalar / vector.w };
	}
	
	friend std::ostream& operator<<(std::ostream& stream, Vector4<T> const& vector) {
		stream << "[" << vector.x << "," << vector.y << "," << vector.z << "," << vector.w << "]";
		return stream;
	}
};

// Typedefs for vector4<T>

typedef Vector4<float> Vector4_float;
typedef Vector4<double> Vector4_double;

}

#endif