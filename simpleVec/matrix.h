//
// This file is part of simpleVec.
//
// (c) 2020 Sébastien Garmier (sebastien.garmier@gmail.com)
//

#ifndef SIMPLEVEC_MATRIX_H
#define SIMPLEVEC_MATRIX_H

#include <simpleVec/vector.h>

#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstring>

/// \file matrix.h
/// \brief This vector library was originally conceived for Brane (see https://sebastiengarmier.ch/brane)

namespace SimpleVec {

#ifndef SIMPLEVEC_USE_ONLY_VECTOR4

// column-major Matrix2<T>

template<typename T>
class Matrix2 {

public:

	/// \brief Entries
	T data[4];

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2() { Identity(); }
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2(T const* data) {
		memcpy(&this->data, data, 4 * sizeof(T));
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2(T const& _0, T const& _1, T const& _2, T const& _3) {
		data[0] = _0;
		data[1] = _1;
		data[2] = _2;
		data[3] = _3;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2(Matrix2<T> const& matrix) {
		memcpy(data, matrix.data, 4 * sizeof(T));
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2(Matrix2<T> const&& matrix) {
		memcpy(data, matrix.data, 4 * sizeof(T));
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	template<typename U>
	operator Matrix2<U>() const {
		Matrix2<U> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret[i] = (U)data[i];
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator=(Matrix2<T> const& matrix) {
		memcpy(data, matrix.data, 4 * sizeof(T));
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator=(Matrix2<T> const&& matrix) {
		memcpy(data, matrix.data, 4 * sizeof(T));
		return *this;
	}

#if SIMPLEVEC_USE_CUDA
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator==(Matrix2<T> const& matrix) const {
		bool b = true;
		for(uint32_t i = 0; i < 4; i++) {
			b &= data[i] == matric.data[i];
		}
		return b;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator!=(Matrix2<T> const& matrix) const {
		bool b = false;
		for(uint32_t i = 0; i < 4; i++) {
			b |= data[i] != matrix.data[i];
		}
		return b;
	}
#else
	bool operator==(Matrix2<T> const& matrix) const {
		bool b = true;
		for(uint32_t i = 0; i < 4; i++) {
			b &= (std::abs(data[i] - matrix.data[i]) < std::numeric_limits<T>::epsilon());
		}
		return b;
	}
	bool operator!=(Matrix2<T> const& matrix) const {
		bool b = false;
		for(uint32_t i = 0; i < 4; i++) {
			b |= (std::abs(data[i] - matrix.data[i]) > std::numeric_limits<T>::epsilon());
		}
		return b;
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	T& At(uint32_t row, uint32_t column) {
		assert(row < 2 && column < 2);
		return data[column * 2 + row];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& At(uint32_t row, uint32_t column) const {
		assert(row < 2 && column < 2);
		return data[column * 2 + row];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T& operator[](uint32_t index) {
		assert(index < 4);
		return data[index];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& operator[](uint32_t index) const {
		assert(index < 4);
		return data[index];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T& operator()(uint32_t row, uint32_t column) {
		return At(row, column);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& operator()(uint32_t row, uint32_t column) const {
		return At(row, column);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> ColumnVec(uint32_t column) const {
		assert(column < 2);
		return{At(0, column), At(1, column)};
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void ColumnVec(uint32_t column, Vector2<T> const& vector) {
		assert(column < 2);
		At(0, column) = vector.m_0;
		At(1, column) = vector.m_1;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> RowVec(uint32_t row) const {
		assert(row < 2);
		return{ At(row, 0), At(row, 1) };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RowVec(uint32_t row, Vector2<T> const& vector) {
		assert(row < 2);
		At(row, 0) = vector.m_0;
		At(row, 1) = vector.m_1;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	void SwapElements(uint32_t index1, uint32_t index2) {
		assert(index1 < 4 && index2 < 4);
		T temp = data[index1];
		data[index1] = data[index2];
		data[index2] = temp;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator+(Matrix2<T> const& matrix) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] + matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator-(Matrix2<T> const& matrix) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] - matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator*(Matrix2<T> const& matrix) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] * matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator/(Matrix2<T> const& matrix) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] / matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator-() const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = -data[i];
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator+=(Matrix2<T> const& matrix) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] += matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator-=(Matrix2<T> const& matrix) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] -= matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator*=(Matrix2<T> const& matrix) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] *= matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator/=(Matrix2<T> const& matrix) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] /= matrix.data[i];
		}
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator+(T const& scalar) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] + scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator-(T const& scalar) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] - scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator*(T const& scalar) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] * scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator/(T const& scalar) const {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = data[i] / scalar;
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator+=(T const& scalar) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] += scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator-=(T const& scalar) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] -= scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator*=(T const& scalar) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] *= scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T>& operator/=(T const& scalar) {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] /= scalar;
		}
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	void Zero() {
		for(uint32_t i = 0; i < 4; i++) {
			data[i] = 0;
		}
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Identity() {
		Zero();
		data[0] = 1;
		data[3] = 1;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Scale(T const& xScale, T const& yScale) {
		Zero();
		data[0] = xScale;
		data[3] = yScale;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Rotation(T const& angle) {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif
	
		data[0] = c;
		data[1] = s;
		data[2] = -s;
		data[3] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Rotation(CardinalAngle angle) {
		T c = (T)CardinalCos(angle);
		T s = (T)CardinalSin(angle);

		data[0] = c;
		data[1] = s;
		data[2] = -s;
		data[3] = c;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix2<T> CreateZero() {
		Matrix2<T> ret;
		ret.Zero();
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix2<T> CreateIdentity() {
		Matrix2<T> ret;
		ret.Identity();
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix2<T> CreateScale(T const& xScale, T const& yScale) {
		Matrix2<T> ret;
		ret.Scale(xScale, yScale);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix2<T> CreateRotation(T const& angle) {
		Matrix2<T> ret;
		ret.Rotation(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix2<T> CreateRotation(CardinalAngle angle) {
		Matrix2<T> ret;
		ret.Rotation(angle);
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> Transpose() const {
		Matrix2<T> ret;
		ret[0] = data[0];
		ret[1] = data[2];
		ret[2] = data[1];
		ret[3] = data[3];
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T Determinant() const {
		return data[0] * data[3] - data[2] * data[1];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> Inverse() const {
		T det = Determinant();
		if(det == T(0)) return Matrix2<T>::CreateZero();

		Matrix2<T> ret;
		ret[0] = data[3];
		ret[1] = -data[1];
		ret[2] = -data[2];
		ret[3] = data[0];
		
		return ret / det;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> Mult(Matrix2<T> const& matrix) const {
		Matrix2<T> ret;
		ret.data[0] = data[0] * matrix.data[0] + data[2] * matrix.data[1];
		ret.data[1] = data[1] * matrix.data[0] + data[3] * matrix.data[1];
		ret.data[2] = data[0] * matrix.data[2] + data[2] * matrix.data[3];
		ret.data[3] = data[1] * matrix.data[2] + data[3] * matrix.data[3];
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> Mult(Vector2<T> const& vector) const {
		return{ data[0] * vector.x + data[2] * vector.y, data[1] * vector.x + data[3] * vector.y };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix2<T> operator|(Matrix2<T> const& matrix) const {
		return Mult(matrix);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector2<T> operator|(Vector2<T> const& vector) const {
		return Mult(vector);
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T*() {
		return (T*)data;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T const*() {
		return (T const*)data;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix2<T> operator+(T const& scalar, Matrix2<T> const& matrix) {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = scalar + matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix2<T> operator-(T const& scalar, Matrix2<T> const& matrix) {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = scalar - matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix2<T> operator*(T const& scalar, Matrix2<T> const& matrix) {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = scalar * matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix2<T> operator/(T const& scalar, Matrix2<T> const& matrix) {
		Matrix2<T> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret.data[i] = scalar / matrix.data[i];
		}
		return ret;
	}

	friend std::ostream& operator<<(std::ostream& stream, Matrix2<T> const& matrix) {
		std::string entries[4];
		uint32_t widths[2] = { 0, 0 };
		std::ostringstream oss;
		for(uint32_t i = 0; i < 4; i++) {
			oss.clear();
			oss.str("");
			oss << matrix.data[i];
			entries[i] = oss.str();
			widths[i / 2] = std::max(widths[i / 2], (uint32_t)entries[i].size());
		}

		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[0] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[2] << " |" << std::endl;
		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[1] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[3] << " |";
		return stream;
	}
};

// Typedefs for Matrix2<T>

typedef Matrix2<float> Matrix2_float;
typedef Matrix2<double> Matrix2_double;

// column-major Matrix3<T>

template<typename T>
class Matrix3 {

public:

	T data[9];

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3() { Identity(); }
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3(T const* data) {
		memcpy(&this->data, data, 9 * sizeof(T));
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3(T const& _0, T const& _1, T const& _2,
		T const& _3, T const& _4, T const& _5,
		T const& _6, T const& _7, T const& _8) {
		data[0] = _0;
		data[1] = _1;
		data[2] = _2;
		data[3] = _3;
		data[4] = _4;
		data[5] = _5;
		data[6] = _6;
		data[7] = _7;
		data[8] = _8;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3(Matrix3<T> const& matrix) {
		memcpy(data, matrix.data, 9 * sizeof(T));
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3(Matrix3<T> const&& matrix) {
		memcpy(data, matrix.data, 9 * sizeof(T));
	}

	template<typename U>
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator Matrix3<U>() const {
		Matrix3<U> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret[i] = (U)data[i];
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator=(Matrix3<T> const& matrix) {
		memcpy(data, matrix.data, 9 * sizeof(T));
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator=(Matrix3<T> const&& matrix) {
		memcpy(data, matrix.data, 9 * sizeof(T));
		return *this;
	}

#if SIMPLEVEC_USE_CUDA
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator==(Matrix3<T> const& matrix) const {
		bool b = true;
		for(uint32_t i = 0; i < 9; i++) {
			b &= data[i] == matric.data[i];
		}
		return b;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator!=(Matrix3<T> const& matrix) const {
		bool b = false;
		for(uint32_t i = 0; i < 9; i++) {
			b |= data[i] != matrix.data[i];
		}
		return b;
	}
#else
	bool operator==(Matrix3<T> const& matrix) const {
		bool b = true;
		for(uint32_t i = 0; i < 9; i++) {
			b &= (std::abs(data[i] - matrix.data[i]) < std::numeric_limits<T>::epsilon());
		}
		return b;
	}
	bool operator!=(Matrix3<T> const& matrix) const {
		bool b = false;
		for(uint32_t i = 0; i < 9; i++) {
			b |= (std::abs(data[i] - matrix.data[i]) > std::numeric_limits<T>::epsilon());
		}
		return b;
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	T& At(uint32_t row, uint32_t column) {
		assert(row < 3 && column < 3);
		return data[column * 3 + row];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& At(uint32_t row, uint32_t column) const {
		assert(row < 3 && column < 3);
		return data[column * 3 + row];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T& operator[](uint32_t index) {
		assert(index < 9);
		return data[index];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& operator[](uint32_t index) const {
		assert(index < 9);
		return data[index];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T& operator()(uint32_t row, uint32_t column) {
		return At(row, column);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& operator()(uint32_t row, uint32_t column) const {
		return At(row, column);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> ColumnVec(uint32_t column) const {
		assert(column < 3);
		return{ At(0, column), At(1, column), At(2, column) };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void ColumnVec(uint32_t column, Vector3<T> const& vector) {
		assert(column < 3);
		At(0, column) = vector.m_0;
		At(1, column) = vector.m_1;
		At(2, column) = vector.m_2;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> RowVec(uint32_t row) const {
		assert(row < 3);
		return{ At(row, 0), At(row, 1), At(row, 2) };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RowVec(uint32_t row, Vector3<T> const& vector) {
		assert(row < 3);
		At(row, 0) = vector.m_0;
		At(row, 1) = vector.m_1;
		At(row, 2) = vector.m_2;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	void SwapElements(uint32_t index1, uint32_t index2) {
		assert(index1 < 9 && index2 < 9);
		T temp = data[index1];
		data[index1] = data[index2];
		data[index2] = temp;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator+(Matrix3<T> const& matrix) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] + matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator-(Matrix3<T> const& matrix) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] - matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator*(Matrix3<T> const& matrix) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] * matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator/(Matrix3<T> const& matrix) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] / matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator-() const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = -data[i];
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator+=(Matrix3<T> const& matrix) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] += matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator-=(Matrix3<T> const& matrix) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] -= matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator*=(Matrix3<T> const& matrix) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] *= matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator/=(Matrix3<T> const& matrix) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] /= matrix.data[i];
		}
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator+(T const& scalar) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] + scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator-(T const& scalar) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] - scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator*(T const& scalar) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] * scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator/(T const& scalar) const {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = data[i] / scalar;
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator+=(T const& scalar) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] += scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator-=(T const& scalar) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] -= scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator*=(T const& scalar) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] *= scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T>& operator/=(T const& scalar) {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] /= scalar;
		}
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	void Zero() {
		for(uint32_t i = 0; i < 9; i++) {
			data[i] = 0;
		}
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Identity() {
		Zero();
		data[0] = 1;
		data[4] = 1;
		data[8] = 1;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Translate(T const xTrans, T const& yTrans) {
		Identity();
		data[6] = xTrans;
		data[7] = yTrans;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Scale(T const& xScale, T const& yScale, T const& zScale) {
		Zero();
		data[0] = xScale;
		data[4] = yScale;
		data[8] = zScale;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Rotation(T const& angle, Vector3<T> const& axis) {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif
		T _c = 1 - c;

		T xy = axis.x * axis.y;
		T xz = axis.x * axis.z;
		T yz = axis.y * axis.z;

		data[0] = c + axis.x * axis.x * _c;
		data[1] = xy * _c + axis.z * s;
		data[2] = xz * _c - axis.y * s;

		data[3] = xy * _c - axis.z * s;
		data[4] = c + axis.y * axis.y * _c;
		data[5] = yz * _c + axis.x * s;

		data[6] = xz * _c + axis.y * s;
		data[7] = yz * _c - axis.x * s;
		data[8] = c + axis.z * axis.z * _c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationX(T const& angle) {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		Identity();
		data[4] = c;
		data[5] = s;
		data[7] = -s;
		data[8] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationX(CardinalAngle angle) {
		T c = (T)CardinalCos(angle);
		T s = (T)CardinalSin(angle);

		Identity();
		data[4] = c;
		data[5] = s;
		data[7] = -s;
		data[8] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationY(T const& angle) {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		Identity();
		data[0] = c;
		data[2] = -s;
		data[6] = s;
		data[8] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationY(CardinalAngle angle) {
		T c = (T)CardinalCos(angle);
		T s = (T)CardinalSin(angle);

		Identity();
		data[0] = c;
		data[2] = -s;
		data[6] = s;
		data[8] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationZ(T const& angle) {
		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		Identity();
		data[0] = c;
		data[1] = s;
		data[3] = -s;
		data[4] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationZ(CardinalAngle angle) {
		T c = (T)CardinalCos(angle);
		T s = (T)CardinalSin(angle);

		Identity();
		data[0] = c;
		data[1] = s;
		data[3] = -s;
		data[4] = c;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateZero() {
		Matrix3<T> ret;
		ret.Zero();
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateIdentity() {
		Matrix3<T> ret;
		ret.Identity();
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateTranslation(T const& xTrans, T const& yTrans) {
		Matrix3<T> ret;
		ret.Translate(xTrans, yTrans);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateScale(T const& xScale, T const& yScale, T const& zScale) {
		Matrix3<T> ret;
		ret.Scale(xScale, yScale, zScale);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateRotation(T const& angle, Vector3<T> const& axis) {
		Matrix3<T> ret;
		ret.Rotation(angle, axis);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateRotationX(T const& angle) {
		Matrix3<T> ret;
		ret.RotationX(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateRotationX(CardinalAngle angle) {
		Matrix3<T> ret;
		ret.RotationX(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateRotationY(T const& angle) {
		Matrix3<T> ret;
		ret.RotationY(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateRotationY(CardinalAngle angle) {
		Matrix3<T> ret;
		ret.RotationY(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateRotationZ(T const& angle) {
		Matrix3<T> ret;
		ret.RotationZ(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix3<T> CreateRotationZ(CardinalAngle angle) {
		Matrix3<T> ret;
		ret.RotationZ(angle);
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> Transpose() const {
		Matrix3<T> ret;
		ret = *this;
		ret.SwapElements(1, 3);
		ret.SwapElements(2, 6);
		ret.SwapElements(5, 7);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T Determinant() const {
		return data[0] * data[4] * data[8]
			+ data[3] * data[7] * data[2]
			+ data[6] * data[1] * data[5]
			- data[6] * data[4] * data[2]
			- data[0] * data[7] * data[5]
			- data[3] * data[1] * data[8];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> Inverse() const {
		T det = Determinant();
		if(det == T(0)) return Matrix3<T>::CreateZero();

		Matrix3<T> ret;
		ret[0] = data[4] * data[8] - data[7] * data[5];
		ret[1] = data[7] * data[2] - data[1] * data[8];
		ret[2] = data[1] * data[5] - data[4] * data[2];
		ret[3] = data[6] * data[5] - data[3] * data[8];
		ret[4] = data[0] * data[8] - data[6] * data[2];
		ret[5] = data[3] * data[2] - data[0] * data[5];
		ret[6] = data[3] * data[7] - data[6] * data[4];
		ret[7] = data[6] * data[1] - data[0] * data[7];
		ret[8] = data[0] * data[4] - data[3] * data[1];

		return ret / det;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> Mult(Matrix3<T> const& matrix) const {
		Matrix3<T> ret;
		for(uint32_t row = 0; row < 3; row++) {
			for(uint32_t column = 0; column < 3; column++) {
				ret.data[column * 3 + row] = data[row] * matrix.data[column * 3] + data[3 + row] * matrix.data[column * 3 + 1] + data[6 + row] * matrix.data[column * 3 + 2];
			}
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> Mult(Vector3<T> const& vector) const {
		return{ data[0] * vector.x + data[3] * vector.y + data[6] * vector.z, data[1] * vector.x + data[4] * vector.y + data[7] * vector.z, data[2] * vector.x + data[5] * vector.y + data[8] * vector.z };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix3<T> operator|(Matrix3<T> const& matrix) const {
		return Mult(matrix);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector3<T> operator|(Vector3<T> const& vector) const {
		return Mult(vector);
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T*() {
		return (T*)data;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T const*() {
		return (T const*)data;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix3<T> operator+(T const& scalar, Matrix3<T> const& matrix) {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = scalar + matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix3<T> operator-(T const& scalar, Matrix3<T> const& matrix) {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = scalar - matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix3<T> operator*(T const& scalar, Matrix3<T> const& matrix) {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = scalar * matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix3<T> operator/(T const& scalar, Matrix3<T> const& matrix) {
		Matrix3<T> ret;
		for(uint32_t i = 0; i < 9; i++) {
			ret.data[i] = scalar / matrix.data[i];
		}
		return ret;
	}

	friend std::ostream& operator<<(std::ostream& stream, Matrix3<T> const& matrix) {
		std::string entries[9];
		uint32_t widths[3] = { 0, 0, 0 };
		std::ostringstream oss;
		for(uint32_t i = 0; i < 9; i++) {
			oss.clear();
			oss.str("");
			oss << matrix.data[i];
			entries[i] = oss.str();
			widths[i / 3] = std::max(widths[i / 3], (uint32_t)entries[i].size());
		}

		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[0] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[3] << std::setw(widths[2] + 1) << std::setfill(' ') << entries[6] << " |" << std::endl;
		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[1] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[4] << std::setw(widths[2] + 1) << std::setfill(' ') << entries[7] << " |" << std::endl;
		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[2] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[5] << std::setw(widths[2] + 1) << std::setfill(' ') << entries[8] << " |";
		return stream;
	}
};

// Typedefs for Matrix3<T>

typedef Matrix3<float> Matrix3_float;
typedef Matrix3<double> Matrix3_double;

#endif

// column-major Matrix4<T>

template<typename T>
class Matrix4 {

public:

	T data[16];

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4() = default;
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4(T const* data) {
		memcpy(&this->data, data, 16 * sizeof(T));
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4(T const& _0, T const& _1, T const& _2, T const& _3,
		T const& _4, T const& _5, T const& _6, T const& _7,
		T const& _8, T const& _9, T const& _10, T const& _11,
		T const& _12, T const& _13, T const& _14, T const& _15) {
		data[0] = _0;
		data[1] = _1;
		data[2] = _2;
		data[3] = _3;
		data[4] = _4;
		data[5] = _5;
		data[6] = _6;
		data[7] = _7;
		data[8] = _8;
		data[9] = _9;
		data[10] = _10;
		data[11] = _11;
		data[12] = _12;
		data[13] = _13;
		data[14] = _14;
		data[15] = _15;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4(Matrix4<T> const& matrix) {
		memcpy(this->data, matrix.data, 16 * sizeof(T));
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4(Matrix4<T> const&& matrix) {
		memcpy(this->data, matrix.data, 16 * sizeof(T));
	}

	template<typename U>
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator Matrix4<U>() const {
		Matrix4<U> ret;
		for(uint32_t i = 0; i < 4; i++) {
			ret[i] = (U)data[i];
		}
		return ret;
	}
	
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator=(Matrix4<T> const& matrix) {
		memcpy(this->data, matrix.data, 16 * sizeof(T));
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator=(Matrix4<T> const&& matrix) {
		memcpy(this->data, matrix.data, 16 * sizeof(T));
		return *this;
	}

#if SIMPLEVEC_USE_CUDA
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator==(Matrix4<T> const& matrix) const {
		bool b = true;
		for(uint32_t i = 0; i < 16; i++) {
			b &= data[i] == matric.data[i];
		}
		return b;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	bool operator!=(Matrix4<T> const& matrix) const {
		bool b = false;
		for(uint32_t i = 0; i < 16; i++) {
			b |= data[i] != matrix.data[i];
		}
		return b;
	}
#else
	bool operator==(Matrix4<T> const& matrix) const {
		bool b = true;
		for(uint32_t i = 0; i < 16; i++) {
			b &= (std::abs(data[i] - matrix.data[i]) < std::numeric_limits<T>::epsilon());
		}
		return b;
	}
	bool operator!=(Matrix4<T> const& matrix) const {
		bool b = false;
		for(uint32_t i = 0; i < 16; i++) {
			b |= (std::abs(data[i] - matrix.data[i]) > std::numeric_limits<T>::epsilon());
		}
		return b;
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	T& At(uint32_t row, uint32_t column) {
		assert(row < 4 && column < 4);
		return data[column * 4 + row];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& At(uint32_t row, uint32_t column) const {
		assert(row < 4 && column < 4);
		return data[column * 4 + row];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T& operator[](uint32_t index) {
		assert(index < 16);
		return data[index];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& operator[](uint32_t index) const {
		assert(index < 16);
		return data[index];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T& operator()(uint32_t row, uint32_t column) {
		return At(row, column);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T const& operator()(uint32_t row, uint32_t column) const {
		return At(row, column);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> ColumnVec(uint32_t column) const {
		assert(column < 4);
		return{ At(0, column), At(1, column), At(2, column), At(3, column) };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void ColumnVec(uint32_t column, Vector4<T> const& vector) {
		assert(column < 4);
		At(0, column) = vector.m_0;
		At(1, column) = vector.m_1;
		At(2, column) = vector.m_2;
		At(3, column) = vector.m_3;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> RowVec(uint32_t row) const {
		assert(row < 4);
		return{ At(row, 0), At(row, 1), At(row, 2), At(row, 3) };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RowVec(uint32_t row, Vector4<T> const& vector) {
		assert(row < 4);
		At(row, 0) = vector.m_0;
		At(row, 1) = vector.m_1;
		At(row, 2) = vector.m_2;
		At(row, 3) = vector.m_3;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	void SwapElements(uint32_t index1, uint32_t index2) {
		assert(index1 < 16 && index2 < 16);
		T temp = data[index1];
		data[index1] = data[index2];
		data[index2] = temp;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator+(Matrix4<T> const& matrix) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] + matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator-(Matrix4<T> const& matrix) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] - matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator*(Matrix4<T> const& matrix) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] * matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator/(Matrix4<T> const& matrix) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] / matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator-() const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = -data[i];
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator+=(Matrix4<T> const& matrix) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] += matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator-=(Matrix4<T> const& matrix) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] -= matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator*=(Matrix4<T> const& matrix) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] *= matrix.data[i];
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator/=(Matrix4<T> const& matrix) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] /= matrix.data[i];
		}
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator+(T const& scalar) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] + scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator-(T const& scalar) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] - scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator*(T const& scalar) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] * scalar;
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator/(T const& scalar) const {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = data[i] / scalar;
		}
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator+=(T const& scalar) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] += scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator-=(T const& scalar) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] -= scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator*=(T const& scalar) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] *= scalar;
		}
		return *this;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T>& operator/=(T const& scalar) {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] /= scalar;
		}
		return *this;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	void Zero() {
		for(uint32_t i = 0; i < 16; i++) {
			data[i] = 0;
		}
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Identity() {
		Zero();
		data[0] = 1;
		data[5] = 1;
		data[10] = 1;
		data[15] = 1;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Translate(T const& xTrans, T const& yTrans, T const& zTrans) {
		Identity();
		data[12] = xTrans;
		data[13] = yTrans;
		data[14] = zTrans;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Scale(T const& xScale, T const& yScale, T const& zScale, T const& wScale = 1) {
		Zero();
		data[0] = xScale;
		data[5] = yScale;
		data[10] = zScale;
		data[15] = wScale;
	}

#ifndef SIMPLEVEC_USE_ONLY_VECTOR4
	SIMPLEVEC_CUDA_HOST_DEVICE
	void Rotate(T const& angle, Vector3<T> const& axis) {
		Identity();

		T s, c;
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif
	
		T _c = 1 - c;

		T xy = axis.x * axis.y;
		T xz = axis.x * axis.z;
		T yz = axis.y * axis.z;

		data[0] = c + axis.x * axis.x * _c;
		data[1] = xy * _c + axis.z * s;
		data[2] = xz * _c - axis.y * s;

		data[4] = xy * _c - axis.z * s;
		data[5] = c + axis.y * axis.y * _c;
		data[6] = yz * _c + axis.x * s;

		data[8] = xz * _c + axis.y * s;
		data[9] = yz * _c - axis.x * s;
		data[10] = c + axis.z * axis.z * _c;
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationX(T const& angle) {
		T s, c;	
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		Identity();
		data[5] = c;
		data[6] = s;
		data[9] = -s;
		data[10] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationX(CardinalAngle angle) {
		T c = (T)CardinalCos(angle);
		T s = (T)CardinalSin(angle);

		Identity();
		data[5] = c;
		data[6] = s;
		data[9] = -s;
		data[10] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationY(T const& angle) {
		T s, c;	
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		Identity();
		data[0] = c;
		data[2] = -s;
		data[8] = s;
		data[10] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationY(CardinalAngle angle) {
		T c = (T)CardinalCos(angle);
		T s = (T)CardinalSin(angle);

		Identity();
		data[0] = c;
		data[2] = -s;
		data[8] = s;
		data[10] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationZ(T const& angle) {
		T s, c;	
	#ifdef __CUDA_ARCH__
		sincos(angle, &s, &c);
	#else
		s = std::sin(angle);
		c = std::cos(angle);
	#endif

		Identity();
		data[0] = c;
		data[1] = s;
		data[4] = -s;
		data[5] = c;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	void RotationZ(CardinalAngle angle) {
		T c = (T)CardinalCos(angle);
		T s = (T)CardinalSin(angle);

		Identity();
		data[0] = c;
		data[1] = s;
		data[4] = -s;
		data[5] = c;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateZero() {
		Matrix4<T> ret;
		ret.Zero();
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateIdentity() {
		Matrix4<T> ret;
		ret.Identity();
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateTranslation(T const& xTrans, T const& yTrans, T const& zTrans) {
		Matrix4<T> ret;
		ret.Translate(xTrans, yTrans, zTrans);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateScale(T const& xScale, T const& yScale, T const& zScale) {
		Matrix4<T> ret;
		ret.Scale(xScale, yScale, zScale);
		return ret;
	}

#ifndef SIMPLEVEC_USE_ONLY_VECTOR4
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateRotation(T const& angle, Vector3<T> const& axis) {
		Matrix4<T> ret;
		ret.Rotate(angle, axis);
		return ret;
	}
#endif

	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateRotationX(T const& angle) {
		Matrix4<T> ret;
		ret.RotationX(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateRotationX(CardinalAngle angle) {
		Matrix4<T> ret;
		ret.RotationX(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateRotationY(T const& angle) {
		Matrix4<T> ret;
		ret.RotationY(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateRotationY(CardinalAngle angle) {
		Matrix4<T> ret;
		ret.RotationY(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateRotationZ(T const& angle) {
		Matrix4<T> ret;
		ret.RotationZ(angle);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	static Matrix4<T> CreateRotationZ(CardinalAngle angle) {
		Matrix4<T> ret;
		ret.RotationZ(angle);
		return ret;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> Transpose() const {
		Matrix4<T> ret;
		ret = *this;
		ret.SwapElements(1, 4);
		ret.SwapElements(2, 8);
		ret.SwapElements(3, 12);
		ret.SwapElements(6, 9);
		ret.SwapElements(7, 13);
		ret.SwapElements(11, 14);
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	T Determinant() const {
		return
			data[12] * data[9] * data[6] * data[3] - data[8] * data[13] * data[6] * data[3] -
			data[12] * data[5] * data[10] * data[3] + data[4] * data[13] * data[10] * data[3] +
			data[8] * data[5] * data[14] * data[3] - data[4] * data[9] * data[14] * data[3] -
			data[12] * data[9] * data[2] * data[7] + data[8] * data[13] * data[2] * data[7] +
			data[12] * data[1] * data[10] * data[7] - data[0] * data[13] * data[10] * data[7] -
			data[8] * data[1] * data[14] * data[7] + data[0] * data[9] * data[14] * data[7] +
			data[12] * data[5] * data[2] * data[11] - data[4] * data[13] * data[2] * data[11] -
			data[12] * data[1] * data[6] * data[11] + data[0] * data[13] * data[6] * data[11] +
			data[4] * data[1] * data[14] * data[11] - data[0] * data[5] * data[14] * data[11] -
			data[8] * data[5] * data[2] * data[15] + data[4] * data[9] * data[2] * data[15] +
			data[8] * data[1] * data[6] * data[15] - data[0] * data[9] * data[6] * data[15] -
			data[4] * data[1] * data[10] * data[15] + data[0] * data[5] * data[10] * data[15];
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> Inverse() const {
		Matrix4<T> inv;
		T det;

		inv.data[0] = data[5] * data[10] * data[15] -
			data[5] * data[11] * data[14] -
			data[9] * data[6] * data[15] +
			data[9] * data[7] * data[14] +
			data[13] * data[6] * data[11] -
			data[13] * data[7] * data[10];

		inv.data[4] = -data[4] * data[10] * data[15] +
			data[4] * data[11] * data[14] +
			data[8] * data[6] * data[15] -
			data[8] * data[7] * data[14] -
			data[12] * data[6] * data[11] +
			data[12] * data[7] * data[10];

		inv.data[8] = data[4] * data[9] * data[15] -
			data[4] * data[11] * data[13] -
			data[8] * data[5] * data[15] +
			data[8] * data[7] * data[13] +
			data[12] * data[5] * data[11] -
			data[12] * data[7] * data[9];

		inv.data[12] = -data[4] * data[9] * data[14] +
			data[4] * data[10] * data[13] +
			data[8] * data[5] * data[14] -
			data[8] * data[6] * data[13] -
			data[12] * data[5] * data[10] +
			data[12] * data[6] * data[9];

		inv.data[1] = -data[1] * data[10] * data[15] +
			data[1] * data[11] * data[14] +
			data[9] * data[2] * data[15] -
			data[9] * data[3] * data[14] -
			data[13] * data[2] * data[11] +
			data[13] * data[3] * data[10];

		inv.data[5] = data[0] * data[10] * data[15] -
			data[0] * data[11] * data[14] -
			data[8] * data[2] * data[15] +
			data[8] * data[3] * data[14] +
			data[12] * data[2] * data[11] -
			data[12] * data[3] * data[10];

		inv.data[9] = -data[0] * data[9] * data[15] +
			data[0] * data[11] * data[13] +
			data[8] * data[1] * data[15] -
			data[8] * data[3] * data[13] -
			data[12] * data[1] * data[11] +
			data[12] * data[3] * data[9];

		inv.data[13] = data[0] * data[9] * data[14] -
			data[0] * data[10] * data[13] -
			data[8] * data[1] * data[14] +
			data[8] * data[2] * data[13] +
			data[12] * data[1] * data[10] -
			data[12] * data[2] * data[9];

		inv.data[2] = data[1] * data[6] * data[15] -
			data[1] * data[7] * data[14] -
			data[5] * data[2] * data[15] +
			data[5] * data[3] * data[14] +
			data[13] * data[2] * data[7] -
			data[13] * data[3] * data[6];

		inv.data[6] = -data[0] * data[6] * data[15] +
			data[0] * data[7] * data[14] +
			data[4] * data[2] * data[15] -
			data[4] * data[3] * data[14] -
			data[12] * data[2] * data[7] +
			data[12] * data[3] * data[6];

		inv.data[10] = data[0] * data[5] * data[15] -
			data[0] * data[7] * data[13] -
			data[4] * data[1] * data[15] +
			data[4] * data[3] * data[13] +
			data[12] * data[1] * data[7] -
			data[12] * data[3] * data[5];

		inv.data[14] = -data[0] * data[5] * data[14] +
			data[0] * data[6] * data[13] +
			data[4] * data[1] * data[14] -
			data[4] * data[2] * data[13] -
			data[12] * data[1] * data[6] +
			data[12] * data[2] * data[5];

		inv.data[3] = -data[1] * data[6] * data[11] +
			data[1] * data[7] * data[10] +
			data[5] * data[2] * data[11] -
			data[5] * data[3] * data[10] -
			data[9] * data[2] * data[7] +
			data[9] * data[3] * data[6];

		inv.data[7] = data[0] * data[6] * data[11] -
			data[0] * data[7] * data[10] -
			data[4] * data[2] * data[11] +
			data[4] * data[3] * data[10] +
			data[8] * data[2] * data[7] -
			data[8] * data[3] * data[6];

		inv.data[11] = -data[0] * data[5] * data[11] +
			data[0] * data[7] * data[9] +
			data[4] * data[1] * data[11] -
			data[4] * data[3] * data[9] -
			data[8] * data[1] * data[7] +
			data[8] * data[3] * data[5];

		inv.data[15] = data[0] * data[5] * data[10] -
			data[0] * data[6] * data[9] -
			data[4] * data[1] * data[10] +
			data[4] * data[2] * data[9] +
			data[8] * data[1] * data[6] -
			data[8] * data[2] * data[5];

		det = data[0] * inv.data[0] + data[1] * inv.data[4] + data[2] * inv.data[8] + data[3] * inv.data[12];

		if(det == 0)
			return Matrix4<T>::CreateZero();

		for(uint32_t i = 0; i < 16; i++)
			inv.data[i] /= det;

		return inv;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> Mult(Matrix4<T> const& matrix) const {
		Matrix4<T> ret;
		for(uint32_t row = 0; row < 4; row++) {
			for(uint32_t column = 0; column < 4; column++) {
				ret.data[column * 4 + row] = data[row] * matrix.data[column * 4] + data[4 + row] * matrix.data[column * 4 + 1] + data[8 + row] * matrix.data[column * 4 + 2] + data[12 + row] * matrix.data[column * 4 + 3];
			}
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> Mult(Vector4<T> const& vector) const {
		return{ data[0] * vector.x + data[4] * vector.y + data[8] * vector.z + data[12] * vector.w,
			data[1] * vector.x + data[5] * vector.y + data[9] * vector.z + data[13] * vector.w,
			data[2] * vector.x + data[6] * vector.y + data[10] * vector.z + data[14] * vector.w,
			data[3] * vector.x + data[7] * vector.y + data[11] * vector.z + data[15] * vector.w };
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Matrix4<T> operator|(Matrix4<T> const& matrix) const {
		return Mult(matrix);
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	Vector4<T> operator|(Vector4<T> const& vector) const {
		return Mult(vector);
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T*() {
		return (T*)data;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	operator T const*() {
		return (T const*)data;
	}

	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix4<T> operator+(T const& scalar, Matrix4<T> const& matrix) {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = scalar + matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix4<T> operator-(T const& scalar, Matrix4<T> const& matrix) {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = scalar - matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix4<T> operator*(T const& scalar, Matrix4<T> const& matrix) {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = scalar * matrix.data[i];
		}
		return ret;
	}
	SIMPLEVEC_CUDA_HOST_DEVICE
	friend Matrix4<T> operator/(T const& scalar, Matrix4<T> const& matrix) {
		Matrix4<T> ret;
		for(uint32_t i = 0; i < 16; i++) {
			ret.data[i] = scalar / matrix.data[i];
		}
		return ret;
	}

	friend std::ostream& operator<<(std::ostream& stream, Matrix4<T> const& matrix) {
		std::string entries[16];
		uint32_t widths[4] = { 0, 0, 0, 0 };
		std::ostringstream oss;
		for(uint32_t i = 0; i < 16; i++) {
			oss.clear();
			oss.str("");
			oss << matrix.data[i];
			entries[i] = oss.str();
			widths[i / 4] = std::max(widths[i / 4], (uint32_t)entries[i].size());
		}

		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[0] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[4] << std::setw(widths[2] + 1) << std::setfill(' ') << entries[8] << std::setw(widths[3] + 1) << std::setfill(' ') << entries[12] << " |" << std::endl;
		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[1] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[5] << std::setw(widths[2] + 1) << std::setfill(' ') << entries[9] << std::setw(widths[3] + 1) << std::setfill(' ') << entries[13] << " |" << std::endl;
		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[2] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[6] << std::setw(widths[2] + 1) << std::setfill(' ') << entries[10] << std::setw(widths[3] + 1) << std::setfill(' ') << entries[14] << " |" << std::endl;
		stream << "|" << std::setw(widths[0] + 1) << std::setfill(' ') << entries[3] << std::setw(widths[1] + 1) << std::setfill(' ') << entries[7] << std::setw(widths[2] + 1) << std::setfill(' ') << entries[11] << std::setw(widths[3] + 1) << std::setfill(' ') << entries[15] << " |";

		return stream;
	}
};

// Typedefs for Matrix4<T>

typedef Matrix4<float> Matrix4_float;
typedef Matrix4<double> Matrix4_double;

}

#endif
